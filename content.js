//alert("Hello from your Chrome extension!")

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtYW5hZ2VyX2lkIjoxLCJpYXQiOjE2MTY3NDg3OTJ9.Pjd9S5gA0V8aDm2HqZphcqN6SU0QUXF2B15D9QCpmqE';

const URL = window.location.href.split('?')[0];

const host = {
    'practiceonpointpropertytech.appfolio.com': 'https://manager.fyve-tech.com/v1/',
    'onpointpropertytech.appfolio.com': 'https://manager.fyvetech.com/v1/'
} [window.location.host]



if (window.location.pathname == '/properties') {

    let bc = new BroadcastChannel('thiefFMchannel');

    let table = document.querySelector('.table.table-striped.table-hover.table-list.table-list--show');

    let els = Array.from(table.querySelectorAll('tbody > tr > td > a'));
    let length = els.length;



    //els.map(el => window.open(el.href))

    window.open(els[0].href + '#property_photos_show')
    bc.onmessage = msg => {
        length = length - 1;
        if (length == 0) {
            let button = document.querySelector('.js-pagination-next.pagination__next > a');
            if (!button) return alert('Snatch!');

            window.open(button.href)

        } else {
            let newTab = window.open(els[els.length - length].href + '#property_photos_show', '_blank');
            newTab.focus()
        }
    }


}
if (/^(\/properties\/)\d/.test(window.location.pathname)) {

    console.log('here');

    let newChannel = new BroadcastChannel('new_channel');

    document.body.onload = () => {

        console.log('loaded')

        let bc = new BroadcastChannel('thiefFMchannel');

        let propId = window.location.pathname.split('/').reverse()[0];

        function findPics(page = 1) {
            setTimeout(() => {



                window.scroll(0, 1000 * page);
                let pics = Array.from(document.querySelectorAll('.js-attached-photo-previewable-image > img'));
                let pdfs = Array.from(document.querySelectorAll('#property_attachments_area table tbody td a'));
                if (pics == 0 || !document.querySelector('#property_attachments_area')) return findPics(page + 1);

                const formData = new FormData();



                fetch(`${host}manager/1/documents/image?propId=${propId}`, {
                        method: 'POST',
                        credentials: 'include',
                        body: JSON.stringify(pics.map((pic, i) => ({ src: pic.src, fileName: `${propId}_____${i}.jpeg` }))),
                        headers: new Headers({
                            authorization: token,
                            'Content-Type': 'application/json'
                        })
                    })
                    .then(response => response.json())
                    .then(async response => !pdfs.length ?
                        bc.postMessage('wasted') :
                        await Promise.all(pdfs.map((pdf, i) => fetch(pdf.href)
                            .then(res => res.blob())
                            .then(blob => {
                                let file = new File([blob], `${propId}_____${i}.pdf`, {
                                    type: 'application/pdf'
                                })
                                formData.append(`${propId}_____${i}.pdf`, file)
                            })))
                        .then(() => fetch(`${host}manager/1/documents/pdf?propId=${propId}`, {
                                method: 'POST',
                                credentials: 'include',
                                body: formData,
                                headers: new Headers({
                                    authorization: token,
                                })
                            })
                            .then(() => bc.postMessage('wasted'))
                            .then(() => window.close())))


                // .then(() => fetch(`${host}manager/1/documents/image`, {
                //         method: 'POST',
                //         credentials: 'include',
                //         body: formData,
                //         headers: new Headers({
                //             authorization: token
                //         })
                //     })
                //     .then(response => response.json())
                //     .then(response => {

                //         console.log({ response })

                //     }))

            }, 5000);
        };
        findPics(1)

    }

}

if (/image/.test(window.location.pathname)) {

    // let newChannel = new BroadcastChannel('newChannel');

    // debug();

    // newChannel.onmessage(({message}) => {
    document.body.onload = () => {
        function callClickEvent(element) {
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("click", true, true);
            element.dispatchEvent(evt);
        };

        callClickEvent(document.querySelector('img'));
        setTimeout(() => callClickEvent(document.querySelector('img')), 30)

        //message = JSON.parse(message);
        let img = document.querySelector('img');

        fetch(img.src, {
                mode: 'no-cors'
            })
            .then(response => response.clone())
            .then(response => response.blob())
            .then(blob => {
                const formData = new FormData();

                console.log({ blob })
                let fileName = message.fileName;

                let file = new File([blob], fileName, {
                    type: 'image/jpeg',
                    size: blob.size
                });

                formData.append(fileName, file);



                fetch(`${host}manager/1/documents/image`, {
                        method: 'POST',
                        credentials: 'include',
                        body: formData,
                        headers: new Headers({
                            authorization: token
                        })
                    })
                    .then(response => response.json())
                    .then(response => newChannel.postMessage(fileName))

            })

    }
    // })

}